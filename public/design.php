<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8" />
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Impress Printers</title>
  <link rel="stylesheet" href="css/app.css" />
  <script src="bower_components/modernizr/modernizr.js"></script>
</head>
<body>  
   
   
    <div class="row toprow" style="background-color: #6799c8;/* max-width: 100%; width: 100%; margin: 0rem; */height:2em;">
      <div class="medium-12 column">
      </div>
    </div>

   <div class="row" style="margin-top:1em;">
      <div class="medium-12 column">


        <nav class="top-bar" data-topbar role="navigation">
          <ul class="title-area" style="height:6em;">
            <li class="name">
              <img src="images/ImpressLogo.jpg"/>
              <!--<h1><a href="#">ImpressPrinters</a></h1>-->
            </li>
       <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
            <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
          </ul>

          <section class="top-bar-section">
    <!-- Right Nav Section -->
            <ul class="right">
              <li class="active"><a href="index.php">Home</a></li>
              <li class="active"><a href="aboutus.php">About Us</a></li>
              <li class="has-dropdown">
                <a href="services.php">Services</a>
                  <ul class="dropdown">
                    <li class="active"><a href="design.php">Design</a></li>
                    <li class="#"><a href="printing.php">Printing</a></li>
                  </ul>
              </li>            
              <li class="has-dropdown">
                <a href="products.php">Products</a>
                 <ul class="dropdown">
                    <li class="active"><a href="businesscards.php">Business Cards</a></li>
                    <li class="#"><a href="notepads.php">Notepads</a></li>
                    <li class="#"><a href="postcards.php">Postcards</a></li>
                    <li class="#"><a href="catalouges.php">Catalouges</a></li>
                    <li class="#"><a href="presentation-folders.php">Presentation Folders</a></li>
                    <li class="#"><a href="letterhead.php">Letterhead</a></li>
                    <li class="#"><a href="envelopes.php">Envelopes</a></li>
                    <li class="#"><a href="brochures.php">Brochures</a></li>
                    <li class="#"><a href="flyers.php">Flyers and Sell Sheets</a></li>
                    <li class="#"><a href="carbonless-forms.php">Carbonless Forms</a></li>
                  </ul>
              </li>
              <li class="active"><a href="contactus.php">Contact</a></li>
      
      
            </ul>   
    
          </section>
      </nav>

      </div>
    </div>

    <div class="row" style="margin-top:1em;">
      <div class="medium-5 column" style="margin-top:1em;">
        <img src="images/Design.jpg">
      </div>
      <div class="medium-7 column">
        <h5 class="services-heading">Smart, Practical, Down-to-Earth Graphic Design</h5>
        <p>Effective graphic design makes the most of resources and materials, and when combined with good planning ensures optimum results. At Impress Printers we’ve helped many businesses communicate their brand with impressive, professional design.</p>
        <p>Our team of professional graphic designers have extensive experience in the design of exciting and successful visual communications. It requires specialized knowledge and skill to design correctly for print.</p>
        <p>Accustomed to the no-nonsense environment of the printing industry – with its technical constraints, short deadlines and broad range of clientele – we offer a fast, practical, down-to-earth professional design service.</p>
        <p>Please contact us today to book an appointment to discuss how Impress Printers can help you</p>
        <p>Do you have a concept? Allow us to transform your idea into reality and create your unique marketing edge.</p>
      </div>
    </div>



    <div class="row footer-row">
      <div class="medium-4 footer-col column">
        <h4 class="text-center footer-text">Impress Printers</h4>
        <p class="text-center footer-text">1366 Spruce Street</p>
        <p class="text-center footer-text">Winnipeg, Manitoba</p>
        <p class="text-center footer-text">Canada, R3E 2V7</p>
      </div>
      <div class="medium-4 footer-col column">
        <h5 class="text-center footer-text"></h5>
          <p class="text-center footer-text">P:204.774.0449</p>
          <p class="text-center footer-text">F:204.775.2432</p>
          <p class="text-center footer-text"><a href="mailto:info@impressprinters.ca">info@impressprinters.ca</a></p>
      </div>
      <div class="medium-4 footer-col column">
        <h5 class="text-center footer-text"></h5>
          <p class="text-center footer-text">Hours of Operation: </p>
          <p class="text-center footer-text">Monday - Friday</p>
          <p class="text-center footer-text">8:30 am - 5:30 pm</p>
      </div>
    </div>

    <div class="row bottom-row"  style="background-color: #6799c8;/* max-width: 100%; width: 100%; margin: 0rem; */height:2em;">
      <div class="medium-12 column">
        <p class="text-center" style="color:gray;margin-bottom:0em;">Copyright © 2015 Impress Printers. All rights reserved. The original designs showcased on this site may not be copied, used or sold</p>
      </div>
    </div>



      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <script src="bower_components/foundation/js/foundation.min.js"></script>
      <script src="js/app.js"></script>
    </body>
    </html>

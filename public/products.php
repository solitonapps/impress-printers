<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8" />
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Impress Printers</title>
  <link rel="stylesheet" href="css/app.css" />
  <script src="bower_components/modernizr/modernizr.js"></script>
</head>
<body>  
   
   
    <div class="row toprow" style="background-color: #6799c8;/* max-width: 100%; width: 100%; margin: 0rem; */height:2em;">
      <div class="medium-12 column">
      </div>
    </div>

   <div class="row" style="margin-top:1em;">
      <div class="medium-12 column">


        <nav class="top-bar" data-topbar role="navigation">
          <ul class="title-area" style="height:6em;">
            <li class="name">
              <img src="images/ImpressLogo.jpg"/>
              <!--<h1><a href="#">ImpressPrinters</a></h1>-->
            </li>
       <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
            <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
          </ul>

          <section class="top-bar-section">
    <!-- Right Nav Section -->
            <ul class="right">
              <li class="active"><a href="index.php">Home</a></li>
              <li class="active"><a href="aboutus.php">About Us</a></li>
              <li class="has-dropdown">
                <a href="services.php">Services</a>
                  <ul class="dropdown">
                    <li class="active"><a href="design.php">Design</a></li>
                    <li class="#"><a href="printing.php">Printing</a></li>
                  </ul>
              </li>            
              <li class="has-dropdown">
                <a href="products.php">Products</a>
                 <ul class="dropdown">
                    <li class="active"><a href="businesscards.php">Business Cards</a></li>
                    <li class="#"><a href="notepads.php">Notepads</a></li>
                    <li class="#"><a href="postcards.php">Postcards</a></li>
                    <li class="#"><a href="catalouges.php">Catalouges</a></li>
                    <li class="#"><a href="presentation-folders.php">Presentation Folders</a></li>
                    <li class="#"><a href="letterhead.php">Letterhead</a></li>
                    <li class="#"><a href="envelopes.php">Envelopes</a></li>
                    <li class="#"><a href="brochures.php">Brochures</a></li>
                    <li class="#"><a href="flyers.php">Flyers and Sell Sheets</a></li>
                    <li class="#"><a href="carbonless-forms.php">Carbonless Forms</a></li>
                  </ul>
              </li>
              <li class="active"><a href="contactus.php">Contact</a></li>
      
      
            </ul>   
    
          </section>
      </nav>

      </div>
    </div>

    <div class="row" style="margin-top:1em;">
      <div class="medium-6 column">
        <div class="row">
          <div class="medium-6 column">
            <img src="images/Brochures.jpg">
          </div>
          <div class="medium-6 column">
            <a href="#"><h5 class="services-heading">Brochures</h5></a>
            <p>A brochure is a quick and resourceful way to transfer pertinent information about your products and services to your target audience.</p>
            <a href="#">MORE INFO</a>
          </div>
        </div>
      </div>
      <div class="medium-6 column">
        <div class="row">
          <div class="medium-6 column">
            <img src="images/Business-Cards.jpg">
          </div>
          <div class="medium-6 column">
            <a href="#"><h5 class="services-heading">Business Cards</h5></a>
            <p>Business cards are one of the most important marketing tools for your business.</p>
            <a href="#">MORE INFO</a>
          </div>
        </div>
      </div>
    </div>

    
    <div class="row" style="margin-top:1em;">
      <div class="medium-6 column">
        <div class="row">
          <div class="medium-6 column">
            <img src="images/Carbonless-Forms.jpg">
          </div>
          <div class="medium-6 column">
            <a href="#"><h5 class="services-heading">Carbonless Forms</h5></a>
            <p>The use of carbonless forms makes it easier for offices and businesses to create and keep duplicate copies of their written documents.</p>
            <a href="#">MORE INFO</a>
          </div>
        </div>
      </div>
      <div class="medium-6 column">
        <div class="row">
          <div class="medium-6 column">
            <img src="images/Catalogues.jpg">
          </div>
          <div class="medium-6 column">
            <a href="#"><h5 class="services-heading">Catalogues</h5></a>
            <p>Catalogues are an excellent tool to educate your customers and showcase your products and services.</p>
            <a href="#">MORE INFO</a>
          </div>
        </div>
      </div>
    </div>

    <div class="row" style="margin-top:1em;">
      <div class="medium-6 column">
        <div class="row">
          <div class="medium-6 column">
            <img src="images/Envelopes.jpg">
          </div>
          <div class="medium-6 column">
            <a href="#"><h5 class="services-heading">Envelopes</h5></a>
            <p>Envelopes are the key to making a good initial impression when mailing important materials.</p>
            <a href="#">MORE INFO</a>
          </div>
        </div>
      </div>
      <div class="medium-6 column">
        <div class="row">
          <div class="medium-6 column">
            <img src="images/Flyers.jpg">
          </div>
          <div class="medium-6 column">
            <a href="#"><h5 class="services-heading">Flyers</h5></a>
            <p>Flyers are an effective visual tool to pass along relevant information and help to sell your products or services.</p>
            <a href="#">MORE INFO</a>
          </div>
        </div>
      </div>
    </div>

    <div class="row" style="margin-top:1em;">
      <div class="medium-6 column">
        <div class="row">
          <div class="medium-6 column">
            <img src="images/Letterhead.jpg">
          </div>
          <div class="medium-6 column">
            <a href="#"><h5 class="services-heading">Letterhead</h5></a>
            <p>A professionally printed custom letterhead showcases your important business documents in proper style.</p>
            <a href="#">MORE INFO</a>
          </div>
        </div>
      </div>
      <div class="medium-6 column">
        <div class="row">
          <div class="medium-6 column">
            <img src="images/Notepads.jpg">
          </div>
          <div class="medium-6 column">
            <a href="#"><h5 class="services-heading">Notepads</h5></a>
            <p>Everyday in offices all over, notes are being taken. Why not have your company’s logo and information on that piece of paper?</p>
            <a href="#">MORE INFO</a>
          </div>
        </div>
      </div>
    </div>

    <div class="row" style="margin-top:1em;">
      <div class="medium-6 column">
        <div class="row">
          <div class="medium-6 column">
            <img src="images/Postcards.jpg">
          </div>
          <div class="medium-6 column">
            <a href="#"><h5 class="services-heading">Postcards</h5></a>
            <p>Postcards are one of the most affordable and effective direct mail tools available.</p>
            <a href="#">MORE INFO</a>
          </div>
        </div>
      </div>
      <div class="medium-6 column">
        <div class="row">
          <div class="medium-6 column">
            <img src="images/Presentation-Folders.jpg">
          </div>
          <div class="medium-6 column">
            <a href="#"><h5 class="services-heading">Presentation Folders</h5></a>
            <p>Presentation folders are an elegant way to present all your company’s marketing materials or other documents in a neat, well organized package.</p>
            <a href="#">MORE INFO</a>
          </div>
        </div>
      </div>
    </div>



    <div class="row footer-row">
      <div class="medium-4 footer-col column">
        <h4 class="text-center footer-text">Impress Printers</h4>
        <p class="text-center footer-text">1366 Spruce Street</p>
        <p class="text-center footer-text">Winnipeg, Manitoba</p>
        <p class="text-center footer-text">Canada, R3E 2V7</p>
      </div>
      <div class="medium-4 footer-col column">
        <h5 class="text-center footer-text"></h5>
          <p class="text-center footer-text">P:204.774.0449</p>
          <p class="text-center footer-text">F:204.775.2432</p>
          <p class="text-center footer-text"><a href="mailto:info@impressprinters.ca">info@impressprinters.ca</a></p>
      </div>
      <div class="medium-4 footer-col column">
        <h5 class="text-center footer-text"></h5>
          <p class="text-center footer-text">Hours of Operation: </p>
          <p class="text-center footer-text">Monday - Friday</p>
          <p class="text-center footer-text">8:30 am - 5:30 pm</p>
      </div>
    </div>

    <div class="row bottom-row"  style="background-color: #6799c8;/* max-width: 100%; width: 100%; margin: 0rem; */height:2em;">
      <div class="medium-12 column">
        <p class="text-center" style="color:gray;margin-bottom:0em;">Copyright © 2015 Impress Printers. All rights reserved. The original designs showcased on this site may not be copied, used or sold</p>
      </div>
    </div>



      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <script src="bower_components/foundation/js/foundation.min.js"></script>
      <script src="js/app.js"></script>
    </body>
    </html>

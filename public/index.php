<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8" />
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Impress-Printers</title>
  <link rel="stylesheet" href="css/app.css" />
  <script src="bower_components/modernizr/modernizr.js"></script>
</head>
<body>  
   
   

    <div class="row toprow" style="background-color: #6799c8;/* max-width: 100%; width: 100%; margin: 0rem; */height:2em;">
      <div class="medium-12 column">
      </div>
    </div>

    <div class="row" style="margin-top:1em;">
      <div class="medium-12 column">


        <nav class="top-bar" data-topbar role="navigation">
          <ul class="title-area" style="height:6em;">
            <li class="name">
              <img src="images/ImpressLogo.jpg"/>
              <!--<h1><a href="#">ImpressPrinters</a></h1>-->
            </li>
       <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
            <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
          </ul>

          <section class="top-bar-section">
    <!-- Right Nav Section -->
            <ul class="right">
              <li class="active"><a href="index.php">Home</a></li>
              <li class="active"><a href="aboutus.php">About Us</a></li>
              <li class="has-dropdown">
                <a href="services.php">Services</a>
                  <ul class="dropdown">
                    <li class="active"><a href="design.php">Design</a></li>
                    <li class="#"><a href="printing.php">Printing</a></li>
                  </ul>
              </li>            
              <li class="has-dropdown">
                <a href="products.php">Products</a>
                 <ul class="dropdown">
                    <li class="active"><a href="businesscards.php">Business Cards</a></li>
                    <li class="#"><a href="notepads.php">Notepads</a></li>
                    <li class="#"><a href="postcards.php">Postcards</a></li>
                    <li class="#"><a href="catalouges.php">Catalouges</a></li>
                    <li class="#"><a href="presentation-folders.php">Presentation Folders</a></li>
                    <li class="#"><a href="letterhead.php">Letterhead</a></li>
                    <li class="#"><a href="envelopes.php">Envelopes</a></li>
                    <li class="#"><a href="brochures.php">Brochures</a></li>
                    <li class="#"><a href="flyers.php">Flyers and Sell Sheets</a></li>
                    <li class="#"><a href="carbonless-forms.php">Carbonless Forms</a></li>
                  </ul>
              </li>
              <li class="active"><a href="contactus.php">Contact</a></li>
      
      
            </ul>   
    
          </section>
      </nav>

      </div>
    </div>

    <div class="row" style="margin-top: 1em;">
        <div class="medium-12 column">
          <div class="orbit-container" style="height:417px;">
            <ul data-orbit data-options="animation:fade; pause_on_hover:false; timer_speed:5000; slide_number:false; animation_speed:500; navigation_arrows:false; bullets:false;">
              <li>
                <img src="images/DesignSliderPhoto1.jpg" alt="slide 1" />
                <div class="orbit-caption">
                 <p>> design</p>
                 <p>Our expertise in graphic design and prepress makes your projects come to life</p>
                </div>
              </li>
              <li class="active">
                <img src="images/PrintSliderPhoto.jpg" alt="slide 2" />
                <div class="orbit-caption">
                  <p>> Print</p>
                 <p>we specialize in providing high quality offset and digital printing</p>
                </div>
              </li>
              <li>
                <img src="images/ImpressSlider3.jpg" alt="slide 3" />
                <div class="orbit-caption">
                  <p>> impress</p>
                 <p>Paying attention to every detail, we make sure the finished product looks its best</p>
                </div>
              </li>
            </ul>
          <div>

        </div>
    </div>
    

    <div class="row sliderow"vstyle="margin-top:1em; margin-bottom: 1em;">
      <div class="medium-12 column" style="border-bottom: 1px dashed grey;">
        <img src="images/design-print-impress-1024x91.jpg"/>
      </div>
    </div>

   

   
    <div class="row">
      <div class="medium-12 col-margin column text-center">
       
      </div>
    </div>

    <div class="row row-services">
      <div class="medium-3 column text-center">
        <a href="products.php"><img src="images/ProductsIcon.jpg">
        <h5 class="services-heading">Products</h5></a>
        <p>Looking for print communication tools for your business? Browse our wide variety of products that can be customized to your specific needs.</p>
      </div>
      <div class="medium-3 column text-center">
        <a href="specials.php"><img src="images/SpecialsIcon.jpg">
        <h5 class="services-heading">Specials</h5></a>
        <p>Check out our special print packages and promotional offers.</p>
      </div>
      <div class="medium-3 column text-center">
        <a href="printing.php"><img src="images/SupportIcon.jpg">
        <h5 class="services-heading">Tips & Support</h5></a>
        <p>Confused about print jargon or just need a little more information about a particular aspect of commercial printing?</p>
      </div>
      <div class="medium-3 column text-center">
        <a href="#"><img src="images/QuoteIcon.jpg">
        <h5 class="services-heading">Request A Quote</h5></a>
        <p>Send us your project information and we will contact you with a quote.</p>
      </div>
    </div>

   <div class="row footer-row">
      <div class="medium-4 footer-col column" style="border-right: 1px dashed white;">
        <h4 class="text-center footer-text">Impress Printers</h4>
        <p class="text-center footer-text">1366 Spruce Street</p>
        <p class="text-center footer-text">Winnipeg, Manitoba</p>
        <p class="text-center footer-text">Canada, R3E 2V7</p>
      </div>
      <div class="medium-4 footer-col column" style="border-right: 1px dashed white;">
        <h5 class="text-center footer-text"><br/></h5>
          <p class="text-center footer-text">P:204.774.0449</p>
          <p class="text-center footer-text">F:204.775.2432</p>
          <p class="text-center footer-text"><a href="mailto:info@impressprinters.ca">info@impressprinters.ca</a></p>
      </div>
      <div class="medium-4 footer-col column">
        <h5 class="text-center footer-text"><br/></h5>
          <p class="text-center footer-text">Hours of Operation: </p>
          <p class="text-center footer-text">Monday - Friday</p>
          <p class="text-center footer-text">8:30 am - 5:30 pm</p>
      </div>
    </div>

    <div class="row bottom-row"  style="background-color: #6799c8;/* max-width: 100%; width: 100%; margin: 0rem;*/">
      <div class="medium-12 column">
        <p class="text-center" style="color:gray;margin-bottom:0em;">Copyright © 2015 Impress Printers. All rights reserved. The original designs showcased on this site may not be copied, used or sold</p>
      </div>
    </div>
    

          


      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <script src="bower_components/foundation/js/foundation.min.js"></script>
      <script src="js/app.js"></script>
    </body>
    </html>

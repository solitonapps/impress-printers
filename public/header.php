    <div class="row">


      <div class="medium-12 col-margin column text-center">

        <div class="contain-to-grid sticky">     

              <nav class="top-bar" data-topbar role="navigation">

                  <ul class="title-area">
                    <li class="name">
                      <h1><a href="index.php">BallyCastle</a></h1>
                    </li>
                    <li class="toggle-topbar menu-icon"><a href="index.php"><span>Menu</span></a></li>
                  </ul>
                  <section class="top-bar-section">
                      <ul class="right">
                          <li><a href="aboutus.php">About</a></li>
                          <li><a href="services.php">Services</a></li>
                          <li class="has-dropdown"><a href="products.php">Products</a>
                            <ul class="dropdown">
                              <li class="has-dropdown"><a href="products.php">Thermofoil</a>
                                <ul class="dropdown">
                                  <li><a href="products.php#route-patterns">Route Patterns</a></li>
                                  <li><a href="products.php#route-profiles">Route Profiles</a></li>
                                  <li><a href="products.php#edge-properties">Edge Properties</a></li>
                                  <li><a href="products.php#panels-frames">Panels & Frames</a></li>
                                  <li><a href="products.php#colours">Colours</a></li><li>
                                  <li><a href="products.php#mouldings">Mouldings</a></li>
                                </ul>
                              </li>
                              <li><a href="acrylic.php">Acrylic</a></li>
                              <li><a href="textured-melamine.php">Textured Melamine</a></li>
                              <li><a href="veneer.php">Veneer</a></li>
                            </ul>
                          </li>
                          <li><a href="gallery.php">Gallery</a></li>
                          <li><a href="contactus.php">ContactUs</a></li>
                          
                      </ul>
                  </section>
              </nav>
        </div>
      </div>       
        
    </div>
<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8" />
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Impress Printers</title>
  <link rel="stylesheet" href="css/app.css" />
  <script src="bower_components/modernizr/modernizr.js"></script>
</head>
<body>  
   
   
    <div class="row toprow" style="background-color: #6799c8;/* max-width: 100%; width: 100%; margin: 0rem; */height:2em;">
      <div class="medium-12 column">
      </div>
    </div>

   <div class="row" style="margin-top:1em;">
      <div class="medium-12 column">


        <nav class="top-bar" data-topbar role="navigation">
          <ul class="title-area" style="height:6em;">
            <li class="name">
              <img src="images/ImpressLogo.jpg"/>
              <!--<h1><a href="#">ImpressPrinters</a></h1>-->
            </li>
       <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
            <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
          </ul>

          <section class="top-bar-section">
    <!-- Right Nav Section -->
            <ul class="right">
              <li class="active"><a href="index.php">Home</a></li>
              <li class="active"><a href="aboutus.php">About Us</a></li>
              <li class="has-dropdown">
                <a href="services.php">Services</a>
                  <ul class="dropdown">
                    <li class="active"><a href="design.php">Design</a></li>
                    <li class="#"><a href="printing.php">Printing</a></li>
                  </ul>
              </li>            
              <li class="has-dropdown">
                <a href="products.php">Products</a>
                 <ul class="dropdown">
                    <li class="active"><a href="businesscards.php">Business Cards</a></li>
                    <li class="#"><a href="notepads.php">Notepads</a></li>
                    <li class="#"><a href="postcards.php">Postcards</a></li>
                    <li class="#"><a href="catalouges.php">Catalouges</a></li>
                    <li class="#"><a href="presentation-folders.php">Presentation Folders</a></li>
                    <li class="#"><a href="letterhead.php">Letterhead</a></li>
                    <li class="#"><a href="envelopes.php">Envelopes</a></li>
                    <li class="#"><a href="brochures.php">Brochures</a></li>
                    <li class="#"><a href="flyers.php">Flyers and Sell Sheets</a></li>
                    <li class="#"><a href="carbonless-forms.php">Carbonless Forms</a></li>
                  </ul>
              </li>
              <li class="active"><a href="contactus.php">Contact</a></li>
      
      
            </ul>   
    
          </section>
      </nav>

      </div>
    </div>

    <div class="row" style="margin-top:1em;">
      <div class="medium-5 column" style="margin-top:1em;">
        <img src="images/About-Us-Page-Image-919x1024.jpg">
      </div>
      <div class="medium-7 column">
        <h5 class="services-heading">Notepads</h5>
        <p>Everyday, in offices all over, notes are being taken. Why not have your company’s logo and information on that piece of paper? A promotional notepad is an ideal tool for marketing because it advertises your company
while providing a useful take-away for potential customers.</p>
        <p>Each promotional notepad features 25, 50 or 100 pages of branding and product placement opportunities in a wide variety of sizes. Think of the top of mind awareness this creates!</p>
       
        <p>Notepads are a creative and different way to keep your branding and contact information handy for existing clients and a potential customer will be exposed to your company’s name over and over.</p>
        <p>Next time you need a new solution to get your company name onto someone’s desk, consider the novel solution of a promotional notepad.</p>
      </div>
    </div>



    <div class="row footer-row">
      <div class="medium-4 footer-col column">
        <h4 class="text-center footer-text">Impress Printers</h4>
        <p class="text-center footer-text">1366 Spruce Street</p>
        <p class="text-center footer-text">Winnipeg, Manitoba</p>
        <p class="text-center footer-text">Canada, R3E 2V7</p>
      </div>
      <div class="medium-4 footer-col column">
        <h5 class="text-center footer-text"></h5>
          <p class="text-center footer-text">P:204.774.0449</p>
          <p class="text-center footer-text">F:204.775.2432</p>
          <p class="text-center footer-text"><a href="mailto:info@impressprinters.ca">info@impressprinters.ca</a></p>
      </div>
      <div class="medium-4 footer-col column">
        <h5 class="text-center footer-text"></h5>
          <p class="text-center footer-text">Hours of Operation: </p>
          <p class="text-center footer-text">Monday - Friday</p>
          <p class="text-center footer-text">8:30 am - 5:30 pm</p>
      </div>
    </div>

    <div class="row bottom-row"  style="background-color: #6799c8;/* max-width: 100%; width: 100%; margin: 0rem; */height:2em;">
      <div class="medium-12 column">
        <p class="text-center" style="color:gray;margin-bottom:0em;">Copyright © 2015 Impress Printers. All rights reserved. The original designs showcased on this site may not be copied, used or sold</p>
      </div>
    </div>



      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <script src="bower_components/foundation/js/foundation.min.js"></script>
      <script src="js/app.js"></script>
    </body>
    </html>

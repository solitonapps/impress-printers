module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    settings: grunt.file.readJSON('settings.json'),

    sass: {
      options: {
        includePaths: ['bower_components/foundation/scss']
      },
      dist: {
        options: {
          outputStyle: 'compressed',
          sourceMap: true,
        },
        files: {
          'public/css/app.css': 'scss/app.scss'
        }
      }
    },

    copy: {
      files: {
        cwd: 'public/',  // set working folder / root to copy
        src: '**/*',           // copy all files and subfolders
        dest: '<%= settings.copyto %>',    // destination folder
        expand: true           // required when using cwd
      },
      includes: {
        cwd: '',  // set working folder / root to copy
        src: ['bower_components/**/*', 'js/**/*'],           // copy all files and subfolders
        dest: '<%= settings.copyto %>',    // destination folder
        expand: true           // required when using cwd
      },      
    },

    watch: {
      grunt: {
        options: {
          reload: true
        },
        files: ['Gruntfile.js']
      },

      sass: {
        files: 'scss/**/*.scss',
        tasks: ['sass','copy']
      },

      html: {
        files: 'public/**/*.php',
        tasks: ['copy']
      }
    }
  });

  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-copy');

  grunt.registerTask('build', ['sass']);
  grunt.registerTask('default', ['build','watch']);
}
